from django.urls import path
from shortener import views


urlpatterns = [
    path('create-link/', views.LinkCreate.as_view(), name="create-link"),
    path('update-link/<int:pk>/', views.LinkUpdate.as_view(), name="update-link"),
    path('create-tracking/', views.TrackingCreate.as_view(), name="create-tracking"),
    path('update-tracking/<int:pk>/', views.TrackingUpdate.as_view(), name="update-tracking"),
    path('list-links/', views.LinkList.as_view(), name="list-links"),
    path('list-trackings/', views.TrackingList.as_view(), name="list-trackings"),
]
