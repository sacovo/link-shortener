import requests
from bs4 import BeautifulSoup

from django.http import JsonResponse

from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from django.views.generic.base import TemplateView
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required

from shortener.models import Domain, Link, TrackingInfo

class Index(TemplateView):
    template_name = 'shortener/index.html'


# Create your views here.
def redirect_view(request, slug):
    domain = get_object_or_404(Domain, base_url=request.get_host())
    link = get_object_or_404(Link, domain=domain, slug=slug)
    if link.tracking_info:
        return render(request, 'shortener/redirect_view.html', {
            'link':link,
            'tracking': link.tracking_info
        })
    return redirect(link.redirect_to)

class LinkCreate(LoginRequiredMixin, CreateView):
    model = Link
    fields = ['redirect_to', 'tracking_info', 'domain', 'slug']

    def get_form(self, form_class=None):
        form = super(LinkCreate, self).get_form(form_class)
        groups = self.request.user.groups.all()
        form.fields['domain'].queryset = Domain.objects.filter(owner__in=groups)
        return form

    def form_valid(self, form):
        tags = fetch_url_params(form.instance.redirect_to)
        for tag in tags:
            form.instance.__dict__.update({tag: tags[tag]})

        return super(CreateView, self).form_valid(form)


class LinkUpdate(UpdateView, LoginRequiredMixin):
    model = Link

    fields = ['redirect_to', 'tracking_info', 'domain', 'slug', 'title', 
              'og_image', 'og_title', 'og_url', 'og_site_name', 'og_type',
              'og_description', 'og_locale', 'twitter_card', 'twitter_image',
              'twitter_description', 'twitter_site']

    def get_form(self, form_class=None):
        form = super(LinkUpdate, self).get_form(form_class)
        groups = self.request.user.groups.all()
        form.fields['domain'].queryset = Domain.objects.filter(owner__in=groups)
        return form

    def get_queryset(self):
        qs = super(UpdateView, self).get_queryset()
        return qs.filter(domain__owner__in=self.request.user.groups.all())

class LinkList(ListView, LoginRequiredMixin):
    model = Link
    
    def get_queryset(self):
        qs = super(ListView, self).get_queryset()
        return qs.filter(domain__owner__in=self.request.user.groups.all())

class TrackingCreate(CreateView, LoginRequiredMixin):
    model = TrackingInfo
    fields = ['name', 'google_analytics', 'facebook_pixel', 'custom_js', 'owner']

    def get_form(self, form_class=None):
        form = super(TrackingCreate, self).get_form(form_class)
        groups = self.request.user.groups.all()
        form.fields['owner'].queryset = groups
        return form

class TrackingUpdate(UpdateView, LoginRequiredMixin):
    model = TrackingInfo
    fields = ['name', 'google_analytics', 'facebook_pixel', 'custom_js', 'owner']

    def get_form(self, form_class=None):
        form = super(TrackingUpdate, self).get_form(form_class)
        groups = self.request.user.groups.all()
        form.fields['owner'].queryset = groups
        return form

    def get_queryset(self):
        qs = super(UpdateView, self).get_queryset()
        return qs.filter(owner__in=self.request.user.groups.all())

class TrackingList(ListView, LoginRequiredMixin):
    model = TrackingInfo
    
    def get_queryset(self):
        qs = super(ListView, self).get_queryset()
        return qs.filter(owner__in=self.request.user.groups.all())


def fetch_url_params(url):
    html = BeautifulSoup(requests.get(url).text)
    mapping = {
        ('og:image', 'og_image'),
        ('og:title', 'og_title'),
        ('og:url', 'og_url'),
        ('og:site_name', 'og_site_name'),
        ('og:description', 'og_description'),
        ('og:locale', 'og_locale'),
        ('twitter:card', 'twitter_card'),
        ('twitter:image', 'twitter_image'),
        ('twitter:description', 'twitter_description'),
        ('twitter:site', 'twitter_site')
    }
    result = {}

    for m in mapping:
        e = html.head.find('meta', attrs={'property': m[0]})
        if e:
            result.update({m[1]: e['content']})
    e = html.head.find('title')
    if e:
        result.update({'title': e.text})
    return result

