from django.db import models
from django.contrib.auth.models import Group
from django.shortcuts import reverse

# Create your models here.

class Domain(models.Model):
    base_url = models.CharField(max_length=30, db_index=True)
    owner = models.ForeignKey(Group, models.CASCADE)

    def __str__(self):
        return self.base_url


class TrackingInfo(models.Model):
    name = models.CharField(max_length=30)
    google_analytics = models.TextField(blank=True)
    facebook_pixel = models.TextField(blank=True) 

    custom_js = models.TextField(blank=True)
    owner = models.ForeignKey(Group, models.CASCADE)

    def get_absolute_url(self):
        return reverse('update-tracking', kwargs={'pk':self.pk})

    def __str__(self):
        return self.name

TWITTER_CARDS = (
    ('summary', 'summary'),
    ('summary_large_image', 'summary_large_image'),
    ('app', 'app'),
    ('player', 'player'),
)


class Link(models.Model):
    redirect_to = models.URLField()
    title = models.CharField(max_length=180, blank=True)

    og_image = models.URLField(blank=True)
    og_title = models.CharField(max_length=280, blank=True)
    og_url = models.URLField(blank=True)
    og_site_name = models.CharField(max_length=280, blank=True)
    og_type = models.CharField(max_length=100, blank=True)
    og_description = models.TextField(blank=True)
    og_locale = models.CharField(max_length=5, blank=True)

    twitter_card = models.CharField(max_length=40, choices=TWITTER_CARDS, blank=True)
    twitter_image = models.URLField(blank=True)
    twitter_image_alt = models.TextField(max_length=420, blank=True)
    twitter_description = models.TextField(blank=True)
    twitter_site = models.CharField(max_length=30)

    tracking_info = models.ForeignKey(TrackingInfo, models.CASCADE, blank=True, null=True)
    domain = models.ForeignKey(Domain, models.CASCADE)

    slug = models.SlugField()

    def get_absolute_url(self):
        return reverse('update-link', kwargs={'pk':self.pk})

    def __str__(self):
        return self.domain.base_url + '/' + self.slug

    class Meta:
        unique_together = ('slug', 'domain')
        indexes = [
                models.Index(fields=['slug', 'domain'])
                ]
