from django.contrib import admin
from shortener.models import Domain, Link, TrackingInfo
# Register your models here.

@admin.register(Domain)
class DomainAdmin(admin.ModelAdmin):
    pass

@admin.register(Link)
class LinkAdmin(admin.ModelAdmin):
    pass

@admin.register(TrackingInfo)
class TrackingAdmin(admin.ModelAdmin):
    pass
